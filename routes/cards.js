const express = require('express');
const path = require('path');
const fs = require('fs');

const cardsPath = path.join(__dirname, '..', 'data', 'cards.json');
const cards = JSON.parse(fs.readFileSync(cardsPath, 'utf8'));

const router = express.Router();

router.get('/', (req, res) => res.send(cards));

module.exports = router;
