const express = require('express');
const routes = require('./routes');

const port = process.env.PORT || 3000;
const app = express();

app.use(express.static('public'));
app.use(routes);

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening on port ${port}`);
});
